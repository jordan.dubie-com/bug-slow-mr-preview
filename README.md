# Description

As a developer, I want to create a large merge request quickly, so I can review my changes in the merge request directly and benefit of the advanced options I have to review large merge requests.

# Problem

When creating a merge request, the branches are compared at the time of the merge request creation and the UI is blocked until the branches diff is being solved. This leads to user waiting dozens of seconds, sometime minutes, to create large merge requests. 

This as a high priority for us because a business line refuses to use GitLab for that. It is one of our top priority for 2023 to onboard them and they are challenging their migration (900 users) because of that behavior since they are heavily working with large merge requests like this.

# How to reproduce


# Ideal situation

The merge request can be created quickly, the branch compare is done asynchronously in the background and don't block the user navigation. 
